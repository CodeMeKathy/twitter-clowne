/*
  Warnings:

  - You are about to drop the column `oAuth_token` on the `Account` table. All the data in the column will be lost.
  - You are about to drop the column `oAuth_token_secret` on the `Account` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[sessionToken]` on the table `Session` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "Account" DROP COLUMN "oAuth_token",
DROP COLUMN "oAuth_token_secret",
ADD COLUMN     "oauth_token" TEXT,
ADD COLUMN     "oauth_token_secret" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "Session_sessionToken_key" ON "Session"("sessionToken");
