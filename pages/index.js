import { useSession } from 'next-auth/react'
import { useRouter } from 'next/router'

export default function Home() {
  const { data: session, status } = useSession()

  const router = useRouter()

  // Check to see if user data is present within current session
  if (status === 'loading') {
    return null
  }
  if (session) {
    router.push('/home')
  }
  return (
    <div>
      <a href='/api/auth/signin'>login</a>
    </div>
  )
}
